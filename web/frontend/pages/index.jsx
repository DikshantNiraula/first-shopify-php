import { useNavigate, TitleBar, Loading } from "@shopify/app-bridge-react";
import {
    Card,
    EmptyState,
    Layout,
    Page,
    SkeletonBodyText,
} from "@shopify/polaris";
import { QRCodeIndex } from "../components";
import { useAuthenticatedFetch } from "../hooks";
import { useEffect } from "react";

export default function HomePage() {
    const navigate = useNavigate();
    const fetch = useAuthenticatedFetch();

    const getCount = async () => {
        try {
            const response = await fetch("/api/custom-count", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            });

            if (response.ok) {
                const data = await response.json();
                console.log("Count:", data.count);
            } else {
                console.error(
                    "Failed to fetch count:",
                    response.status,
                    response.statusText
                );
            }
        } catch (error) {
            console.error("Error:", error);
        }
    };

    useEffect(() => {
        getCount();
    }, []);

    return (
        <Page>
            <TitleBar
                title="QR codes"
                primaryAction={{
                    content: "Create QR code",
                    onAction: () => navigate("/qrcodes/new"),
                }}
            />
        </Page>
    );
}
